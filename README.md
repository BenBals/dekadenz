# Was ist das Dekadenz-Manifest?

Ganz einfach: Es ist Satire. Wir möchten soziale, wirtschaftliche und strukturelle Ungerechtigkeiten auf eine humoristische Weise ansprechen. Dazu haben wir dieses Manifest des fiktiven Autors D. A. verfasst.

Im Namen ist es natürlich vom Manifest der Kommunistischen Partei inspiriert. Wir zielen auf eine Zahl von 99 Thesen in Anlehnung an Luther ab. 

# Wer sind wir?
Dieses Manifest wurde verfasst von Ben Bals und Freunden.

[Zum Manifest](http://dekadenz.club)